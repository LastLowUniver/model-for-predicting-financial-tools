import pandas as pd
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Conv1D, Flatten
from keras.losses import huber_loss
from keras.metrics import mean_squared_error, mean_absolute_error

df = pd.read_csv('GAZP_170530_230607.csv')
X = df[['<OPEN>', '<HIGH>', '<LOW>', '<CLOSE>', '<VOL>']].values
y = df[['<OPEN>', '<HIGH>', '<LOW>', '<CLOSE>']].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

model = Sequential()
model.add(Conv1D(64, kernel_size=2, activation='relu', input_shape=(5, 1)))
model.add(Conv1D(32, kernel_size=2, activation='relu'))
model.add(Flatten())
model.add(Dense(4))


model.compile(loss=huber_loss, optimizer='adam', metrics=[mean_squared_error, mean_absolute_error])

model.fit(X_train.reshape(-1, 5, 1), y_train, epochs=50, batch_size=32)

model.save('model.h5')

loss, mse, mae = model.evaluate(X_test.reshape(-1, 5, 1), y_test)
print('Loss:', loss)
print('RMSE:', mse ** 0.5)

