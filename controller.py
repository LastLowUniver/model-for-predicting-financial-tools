from flask import Flask, request, jsonify
from keras.models import load_model
from flask_cors import CORS
import numpy as np
import random

app = Flask(__name__)
model = load_model('my_model.h5')

CORS(app)

@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json()
    open_price = data['open']
    close_price = data['close']
    high_price = data['high']
    low_price = data['low']
    value = data['value']
    
    X_new = np.array([[open_price, high_price, low_price, close_price, value]])
    y_pred = model.predict(X_new.reshape((X_new.shape[0], X_new.shape[1], 1)))
    
    return jsonify({
        "open": y_pred.tolist()[0][0],
        "high": y_pred.tolist()[0][1],
        "low": y_pred.tolist()[0][2],
        "close": y_pred.tolist()[0][3],
        "value": y_pred.tolist()[0][4]
    })  

if __name__ == '__main__':
    print("Сервер готов, порт 5000")
    app.run(debug=True, port=5000)
    